### Improved JavaScript code

#### Fancy methods for JS

*RubyWay methods*  

**rJust** adds spaces or symbols **at the left side** of a string, or does nothing.  
``` javascript  
s = 'hello'  
s.rJust() # 'hello'  
s.rJust(8) # '   hello'  
s.rJust(8, 'x') # 'xxxhello'  
s.rJust(8, 0) # '000hello'  
'bar'.rJust(6, 'foo') # 'foobar'  
```  

**lJust** adds spaces or symbols **at the right side** of a string, or does nothing.  
``` javascript  
s = 'hello'  
s.lJust() # 'hello'  
s.lJust(8) # 'hello   '  
s.lJust(8, 'x') # 'helloxxx'  
s.lJust(8, 0) # 'hello000'  
'foo'.lJust(6, 'bar') # 'foobar'  
```

