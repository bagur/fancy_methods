// like 'hello'.rJust(8) => '   hello', 'hello'.rJust(8, '_') => '___hello'
String.prototype.rJust = function(num = 1, sym = ' ') {
  num = Math.max( Math.abs( num ), this.length )
  return ( new Array( num + 1 ).join( sym ) + this.valueOf() ).slice( -num )
}

// like 'hello'.lJust(8) => 'hello   ', 'hello'.lJust(8, '_') => 'hello___'
String.prototype.lJust = function(num = 1, sym = ' ') {
  num = Math.max( Math.abs( num ), this.length )
  return ( this.valueOf() + new Array( num + 1 ).join( sym ) ).slice( 0, num )
}
